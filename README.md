# Auth

Demo setup of username / password authentication and JWT based sessions in
Phoenix framework.

I wrote about it in my blog article [Ueberauth and guardian setup for a Phoenix rest API](https://nathansplace.uk/articles/ueberauth-and-guardian-setup-for-a-phoenix-rest-api)

## Building

Install elixir

```sh
  make
  docker-compose up
  docker exec -i auth_postgres_1 psql -U auth -d auth_prod < dump.sql # sub in actual container name
```
